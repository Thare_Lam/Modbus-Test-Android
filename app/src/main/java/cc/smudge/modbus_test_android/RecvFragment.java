package cc.smudge.modbus_test_android;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Arrays;

public class RecvFragment extends Fragment {

    private TextView[] textViewList;
    private final static String ARG_PARAMETER = "parameter";
    private final static String ARG_RECVDATA = "recvdata";
    private final static String ARG_RULER = "ruler";
    private final static String ARG_RIGHT = "right";

    public static RecvFragment newInstance(String[] parameterList, byte[] recvData, int[] ruler, boolean right){
        RecvFragment fragment = new RecvFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArray(ARG_PARAMETER, parameterList);
        bundle.putByteArray(ARG_RECVDATA, recvData);
        bundle.putIntArray(ARG_RULER, ruler);
        bundle.putBoolean(ARG_RIGHT, right);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment, container, false);
        initView(view);
        return view;
    }

    public void initView(View view){
        String[] parameterList = getArguments().getStringArray(ARG_PARAMETER);
        Context context = getActivity();
        LinearLayout rootView = (LinearLayout)view.findViewById(R.id.rootView);

        LinearLayout.LayoutParams llParam = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout.LayoutParams tvParam = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);

        LinearLayout.LayoutParams etParam = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                55, 1.0f);

        textViewList = new TextView[parameterList.length];

        for(int i = 0;i < textViewList.length;i ++) {
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            linearLayout.setLayoutParams(llParam);

            TextView textView = new TextView(context);
            textView.setText(parameterList[i]);
            textView.setTextColor(Color.parseColor("#444444"));
            textView.setLayoutParams(tvParam);

            textViewList[i] = new TextView(context);
            textViewList[i].setTextSize(15);
            textViewList[i].setLayoutParams(etParam);

            linearLayout.addView(textView);
            linearLayout.addView(textViewList[i]);

            rootView.addView(linearLayout);
        }
        setData(getArguments().getByteArray(ARG_RECVDATA),
                getArguments().getIntArray(ARG_RULER),
                getArguments().getBoolean(ARG_RIGHT));
    }

    public void setData(byte[] data, int[] ruler, boolean right){
        int index = 0;
        if(right) {
            textViewList[index].setText("Right");
            textViewList[index].setTextColor(Color.GREEN);
        }else{
            textViewList[index].setText("Exception");
            textViewList[index].setTextColor(Color.RED);
        }
        textViewList[++index].setText(optData(Arrays.copyOfRange(data, 0, 2)));
        textViewList[++index].setText(optData(Arrays.copyOfRange(data, 2, 4)));
        textViewList[++index].setText(optData(Arrays.copyOfRange(data, 4, 6)));
        textViewList[++index].setText(optData(Arrays.copyOfRange(data, 6, 7)));
        textViewList[++index].setText(optData(Arrays.copyOfRange(data, 7, 8)));
        int i = 0, j = 8;
        for(;i < ruler.length;i ++){
            if(ruler[i] == -1){
                if(data[7] == 43){
                    data = Arrays.copyOfRange(data, j, data.length);
                    StringBuffer info;
                    int len;
                    while(data.length > 0){
                        info = new StringBuffer("");
                        len = data[1];
                        for(int k = 2;k < 2 + len;k ++)
                            info.append(Character.toChars(data[k]));
                        textViewList[++index].setText(info.toString());
                        data = Arrays.copyOfRange(data, 2 + len, data.length);
                    }
                }else
                    textViewList[++index].setText(optData(Arrays.copyOfRange(data, j, data.length)));
            }else {
                textViewList[++index].setText(optData(Arrays.copyOfRange(data, j, j + ruler[i])));
                j += ruler[i];
            }
        }
    }

    private String optData(byte[] data){
        StringBuffer hexValue = new StringBuffer("");
        String temp;
        int len;
        for(int i = 0;i < data.length;i ++){
            temp = Integer.toHexString(data[i]);
            len = temp.length();
            if(len > 2)
                hexValue.append(temp.substring(len - 2, len));
            else if(len == 1)
                hexValue.append("0").append(temp);
            else
                hexValue.append(temp);
        }
        return hexValue.toString();
    }

}
