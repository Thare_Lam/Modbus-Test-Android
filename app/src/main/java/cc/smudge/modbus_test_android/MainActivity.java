package cc.smudge.modbus_test_android;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;

public class MainActivity extends Activity {

    private final static int[] paraIdList = {R.array.fc01_send, R.array.fc02_send, R.array.fc03_send,
            R.array.fc04_send, R.array.fc05_send, R.array.fc06_send, R.array.fc0f_send,
            R.array.fc10_send, R.array.fc16_send, R.array.fc17_send, R.array.fc2b_send};
    private final static int HANDLER_TOAST = 0;
    private final static int HANDLER_ACTIVITY = 1;

    private Spinner spinner;
    private EditText ipEditText;
    private EditText portEditText;
    private EditText timeoutEditText;

    private FragmentManager fm;
    private FragmentTransaction ft;
    private SendFragment sendFragment;

    private MyHandler myHandler = new MyHandler();
    private MyRunnalbe myRunnable = new MyRunnalbe();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init(){

        spinner = (Spinner)findViewById(R.id.spinner);
        ipEditText = (EditText)findViewById(R.id.ipEditText);
        portEditText = (EditText)findViewById(R.id.portEditText);
        timeoutEditText = (EditText)findViewById(R.id.timeoutEditText);

        String[] data_list = getResources().getStringArray(R.array.fi_list);
        ArrayAdapter<String> arr_adapter= new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, data_list);
        arr_adapter.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arr_adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sendFragment = SendFragment.newInstance(getResources().getStringArray(paraIdList[position]),
                        getResources().getStringArray(R.array.fi_list2)[position]);
                setFragment(sendFragment);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    private void setFragment(Fragment fragment){
        fm = getFragmentManager();
        ft = fm.beginTransaction();
        ft.replace(R.id.fcFrame, fragment);
        ft.commit();
    }

    public void onClick(View view){
        myRunnable.setSendData(sendFragment.getSendData());
        try {
            Thread thread = new Thread(myRunnable);
            thread.start();
        }catch (Exception e){
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private byte[] toHex(String s){
        if(s.length() == 0){
            uiHandler(HANDLER_TOAST, "Null data");
            return null;
        }
        if(s.length() % 2 != 0) {
            uiHandler(HANDLER_TOAST, "Invaild data");
            return null;
        }
        char c1, c2;
        byte[] b = new byte[s.length() / 2];
        for(int i = 0;i < s.length();i += 2){
            c1 = s.charAt(i);
            c2 = s.charAt(i + 1);
            if((c1 < '0' || (c1 >= '9' && c1 < 'a') || c1 > 'f') &&
                    (c2 < '0' || (c2 >= '9' && c2 < 'a') || c2 > 'f')) {
                uiHandler(HANDLER_TOAST, "Invaild data");
                return null;
            }
            b[i / 2] = new Integer((Integer.parseInt(String.valueOf(c1), 16) * 16
                    + Integer.parseInt(String.valueOf(c2), 16))).byteValue();
        }
        return b;
    }

    private void uiHandler(int opt, Object object){
        Message msg = new Message();
        msg.arg1 = opt;
        msg.obj = object;
        myHandler.sendMessage(msg);
    }

    private class MyHandler extends android.os.Handler{
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.arg1){
                case HANDLER_TOAST:
                    Toast.makeText(MainActivity.this, (String)msg.obj, Toast.LENGTH_LONG).show();
                    break;
                case HANDLER_ACTIVITY:
                    Intent intent = new Intent(MainActivity.this, RecvActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putByteArray("", (byte[]) msg.obj);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    break;
            }
        }
    }

    private class MyRunnalbe implements Runnable{

        private String sendData;
        private volatile boolean lock = false;

        public void setSendData(String sendData){
            this.sendData = sendData;
        }

        @Override
        public void run() {
            if(lock)
                return;
            lock = true;
            try {
                Socket socket = new Socket(ipEditText.getText().toString().trim(),
                        Integer.parseInt(portEditText.getText().toString().trim()));
                socket.setSoTimeout(Integer.parseInt(timeoutEditText.getText().toString().trim()));
                OutputStream output = socket.getOutputStream();
                InputStream input = socket.getInputStream();
                byte[] recvData = new byte[300];
                byte[] sendDataHex = toHex(sendData);
                if(sendDataHex == null)
                    return;
                output.write(sendDataHex);
                int len = input.read(recvData);
                if(len == 0)
                    uiHandler(HANDLER_TOAST, "Time out");
                else if(len == -1)
                    uiHandler(HANDLER_TOAST, "Receive too much");
                else
                    uiHandler(HANDLER_ACTIVITY, Arrays.copyOfRange(recvData, 0, len));
                System.out.println("Thread Over");
            }catch (Exception e){
                uiHandler(HANDLER_TOAST, e.toString());
            }
            finally {
                lock = false;
                return;
            }
        }
    }

}
