package cc.smudge.modbus_test_android;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SendFragment extends Fragment {

    private EditText[] editTextList;
    private final static String ARG_PARAMETER = "parameter";
    private final static String ARG_FUNCTION = "functioncode";

    public static SendFragment newInstance(String[] parameterList, String functionCode){
        SendFragment fragment = new SendFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArray(ARG_PARAMETER, parameterList);
        bundle.putString(ARG_FUNCTION, functionCode);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment, container, false);
        initView(view);
        return view;
    }

    private void initView(View view){
        Context context = getActivity();
        String[] parameterList = getArguments().getStringArray(ARG_PARAMETER);

        LinearLayout rootView = (LinearLayout)view.findViewById(R.id.rootView);

        LinearLayout.LayoutParams llParam = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout.LayoutParams tvParam = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);

        LinearLayout.LayoutParams etParam = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                55, 1.0f);

        editTextList = new EditText[parameterList.length];

        for(int i = 0;i < editTextList.length;i ++) {
            LinearLayout linearLayout = new LinearLayout(context);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            linearLayout.setLayoutParams(llParam);

            TextView textView = new TextView(context);
            textView.setText(parameterList[i]);
            textView.setTextColor(Color.parseColor("#444444"));
            textView.setLayoutParams(tvParam);

            editTextList[i] = new EditText(context);
            editTextList[i].setTextSize(15);
            editTextList[i].setLayoutParams(etParam);

            linearLayout.addView(textView);
            linearLayout.addView(editTextList[i]);

            rootView.addView(linearLayout);
        }
    }

    public String getSendData(){
        StringBuffer sendData = new StringBuffer("");
        String temp = "";
        for(int i = 0;i < editTextList.length;i ++) {
            temp = editTextList[i].getText().toString().trim();
            if ("".equals(temp)) {
                switch (i){
                    case 0:
                    case 1:
                        temp = "0000";
                        break;
                    case 2:
                        int len = 0;
                        for(int j = 3;j < editTextList.length;j ++){
                            if((j == 3 || j == 4) && "".equals(editTextList[j].getText().toString().trim()))
                                len += 2;
                            else
                                len += editTextList[j].getText().toString().trim().length();
                        }
                        temp =toHex(len, 4);
                        break;
                    case 3:
                        temp = "00";
                        break;
                    case 4:
                        temp = getArguments().getString(ARG_FUNCTION);
                        break;
                    default:
                        break;
                }
            }
            sendData.append(temp);
        }
        return sendData.toString();
    }

    private String toHex(int value, int len){
        String hexValue = Integer.toHexString(value / 2);
        if(len > hexValue.length()){
            StringBuffer hexValue2 = new StringBuffer("");
            for(int i = 0;i < len - hexValue.length();i ++)
                hexValue2.append("0");
            hexValue2.append(hexValue);
            return hexValue2.toString();
        }else
            return hexValue;
    }

}
