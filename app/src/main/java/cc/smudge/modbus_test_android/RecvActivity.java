package cc.smudge.modbus_test_android;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;

public class RecvActivity extends Activity {

    private final static int[] paraIdList = {R.array.fc01_recv, R.array.fc02_recv, R.array.fc03_recv,
            R.array.fc04_recv, R.array.fc05_recv, R.array.fc06_recv, R.array.fc0f_recv,
            R.array.fc10_recv, R.array.fc16_recv, R.array.fc17_recv, R.array.fc2b_recv};
    private final static int[] rulerList = {R.array.array_01, R.array.array_02, R.array.array_03,
            R.array.array_04, R.array.array_05, R.array.array_06, R.array.array_0f,
            R.array.array_10, R.array.array_16, R.array.array_17, R.array.array_2b};

    private byte[] recvData;
    private FragmentManager fm;
    private FragmentTransaction ft;
    private RecvFragment recvFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recv);
        init();
    }

    private void init(){
        Intent intent=getIntent();
        Bundle bundle=intent.getExtras();
        recvData=bundle.getByteArray("");
        if(recvData[7] < 0)
            recvFragment = RecvFragment.newInstance(getResources().getStringArray(R.array.exception_recv),
                    recvData, getResources().getIntArray(R.array.array_exception), false);
        else
            recvFragment = RecvFragment.newInstance(getResources().getStringArray(getIdByFC(recvData[7], paraIdList)),
                    recvData, getResources().getIntArray(getIdByFC(recvData[7], rulerList)), true);
        setFragment(recvFragment);
    }

    private int getIdByFC(int fc, int[] list){
        int[] fcList = getResources().getIntArray(R.array.fc_list);
        for(int i = 0;i < fcList.length;i ++)
            if(fc == fcList[i])
                return list[i];
        return 0;
    }

    private void setFragment(Fragment fragment){
        fm = getFragmentManager();
        ft = fm.beginTransaction();
        ft.replace(R.id.fcFrame, fragment);
        ft.commit();
    }

}
